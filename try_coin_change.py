def changeMoney(coin):
    dten = coin // 10
    coin = coin % 10
    dfive = coin // 5
    coin = coin % 5
    dtwo = coin // 2
    coin = coin % 2
    done = coin // 1
    return f"use 10 : {dten} \nuse 5 : {dfive} \nuse 2 : {dtwo} \nuse 1 : {done}"


if __name__ == "__main__":
    c = int(input("Enter Coin: "))
    cm = changeMoney(c)
    print(cm)
