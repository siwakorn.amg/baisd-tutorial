import timeit


def reFibo(n):
    if n < 0:
        print("Incorrect input")

    elif n == 0:
        return 0

    elif n < 3:
        return 1

    else:
        return reFibo(n - 1) + reFibo(n - 2)


def btuFibo(n):
    lst = [0, 1]
    for i in range(2, n + 1):
        lst.append(lst[i - 1] + lst[i - 2])
    return lst[n]


n = int(input("Enter number: "))

fib = reFibo(n)
print(f'Recursive fibonacci {fib} {timeit.timeit("reFibo(n)", globals=globals())}')


fib = btuFibo(n)
print(f'Buttom-up fibonacci {fib} {timeit.timeit("btuFibo(n)", globals=globals())}')
