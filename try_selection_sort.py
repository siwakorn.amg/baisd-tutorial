def selection_sort(data: list) -> list:
    sort_data = data.copy()

    for i in range(len(sort_data)):
        min_idx = i
        for j in range(i + 1, len(sort_data)):
            if sort_data[min_idx] > sort_data[j]:
                min_idx = j
        sort_data[i], sort_data[min_idx] = sort_data[min_idx], sort_data[i]
    return sort_data


if __name__ == "__main__":

    data = list(map(int, input("enter 10 number: ").split()))

    sort_data = selection_sort(data)

    print(sort_data)
