def bubble_sort(data: list) -> list:
    sort_data = data.copy()
    n = len(sort_data)
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            if sort_data[j] > sort_data[j + 1]:
                sort_data[j], sort_data[j + 1] = sort_data[j + 1], sort_data[j]

    return sort_data


if __name__ == "__main__":

    data = list(map(int, input("enter 10 number: ").split()))

    sort_data = bubble_sort(data)

    print(sort_data)
